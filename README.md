RSD - Rancher Stack Deployment
==============================

To ease the pain of re-deploying (or upgrading) a stack on rancher I created a simple tool that does it automatically.
 
Instruction
-----------

* Download the code from url
* Extract it and go to the folder
* <code>python setup.py install</code> (python3 required)

Usage
-----

To work with rsd you need credentials to rancher's api (Access key and secret from /env/1a5/api), rancher's url, environment id and stack id.

Help
----

rsd -h

Save the settings in a folder
-----------------------------

```bash
mkdir test_project
rsd --username USERNAME --password PASSWORD --rancher-endpoint RANCHER_URL --environment-id ENVIRONMENT_ID --stack-id STACK_ID --dir test_project save
```

Available commands
------------------

* get the current settings (docker compose and rancher compose) <code>rsd get_configs --dir test_project</code>
* if you applied to all services rancher label (io.rancher.container.pull_image: always) and you want to redeploy the stack <code>rsd deploy --dir test_project</code>
* if you want to redeploy the stack with the modified settings that are in the --dir folder <code>rsd deploy_with_custom_settings --dir test_project</code>

Warnings
--------

Currently the last option is not working if you add a new sidekick / service in the compose. It might be solved in the new rancher version - 1.1.4.
