"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='rsd',
    version='0.0.1',
    description='RSD - Rancher Stack Deployer',
    long_description=long_description,

    url='None',

    author='Maciej Baranski',
    author_email='maciej.baranski@amsterdam-standard.pl',

    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='rancher deployment api stack tool',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    install_requires=['requests'],

    entry_points={
        'console_scripts': [
            'rsd=src.rsd:main',
        ],
    },
)
