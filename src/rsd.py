import json

import sys

from src.api import API
from src.parser import parser
from src.scenarios import CustomDeployScenario, DeployScenario, GetConfigsScenario


def main(argv=sys.argv[1:]):
    args = parser.parse_args(args=argv)
    kwargs = dict(args._get_kwargs())

    command = kwargs.pop('command')
    current_directory = kwargs.pop('current_directory')
    if current_directory[-1] == '/':
        current_directory = current_directory[:-1]

    if command == 'save':
        with open('{}/.cowboy'.format(current_directory), 'w') as settings:
            json.dump(kwargs, settings)
    else:
        try:
            with open('{}/.cowboy'.format(current_directory), 'r') as settings_file:
                settings = json.load(settings_file)
        except FileNotFoundError:
            settings = {}

        def get_setting(name):
            return kwargs[name] if name not in settings else settings[name]

        username = get_setting('username')
        password = get_setting('password')
        rancher_url = get_setting('rancher_url')
        environment_id = get_setting('environment_id')
        stack_id = get_setting('stack_id')

        api = API(username, password)

        if command == 'deploy':
            scenario = DeployScenario(rancher_url, environment_id, stack_id, api, current_directory)
            scenario.deploy()
        elif command == 'get_configs':
            scenario = GetConfigsScenario(rancher_url, environment_id, stack_id, api, current_directory)
            scenario.get_configs()
        elif command == 'deploy_with_custom_settings':
            scenario = CustomDeployScenario(rancher_url, environment_id, stack_id, api, current_directory)
            scenario.deploy()
