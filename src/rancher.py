class Rancher(object):

    def __init__(self, rancher_url, environment_id, stack_id, api):
        self.stack_url = '{}/projects/{}/environments/{}'.format(rancher_url, environment_id, stack_id)
        self._api = api

    def check_state(self):
        response = self._api.get(self.stack_url)
        return response['state']

    def get_configs(self):
        response = self._api.post('{}/?action=exportconfig'.format(self.stack_url))
        return dict(
            dockerCompose=response['dockerComposeConfig'],
            rancherCompose=response['rancherComposeConfig']
        )

    def upgrade_stack(self, settings):
        response = self._api.post('{}/?action=upgrade'.format(self.stack_url), data=settings)
        return response

    def finish_upgrade(self):
        response = self._api.post('{}/?action=finishupgrade'.format(self.stack_url))
        return response
