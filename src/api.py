import json
import requests


class API(object):

    def __init__(self, username, password):
        self._auth = (username, password)

    def get(self, url, params={}):
        response = requests.get(url, params=params, auth=self._auth)
        return json.loads(response.text)

    def post(self, url, data={}):
        response = requests.post(url, data=data, auth=self._auth)
        return json.loads(response.text)
