import argparse
import textwrap


parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=textwrap.dedent('''\
Possible commands:
    - save (it saves all the credentials and ids to a json file in the specified folder (default is current directory),
            those credentials will be used if no credential/ids arguments are provided)
    - get_configs (downloads docker compose and rancher compose files to the specified folder)
    - deploy (make a upgrade of the whole stack in rancher using the current settings in rancher - useful to pull
      the newer images with the same tag)
    - deploy_with_custom_settings (make an upgrade of the whole stack in rancher using settings that are in current
      directory, useful to upgrade the settings)
'''))
parser.add_argument('--username', dest='username', default=None, help='rancher api username')
parser.add_argument('--password', dest='password', default=None, help='rancher api password')
parser.add_argument('--rancher-endpoint', dest='rancher_url', default=None, help='rancher endpoint url')
parser.add_argument('--environment-id', dest='environment_id', default=None, help='rancher environment id')
parser.add_argument('--stack-id', dest='stack_id', default=None, help='rancher stack id')
parser.add_argument('--dir', dest='current_directory', default='.',
                    help='current_directory (use it for multiple stacks)')
parser.add_argument('command', help='possible commands: save, get_configs, deploy, deploy_with_custom_settings')
