import time

from src.rancher import Rancher


class Scenario(object):

    def __init__(self, rancher_url, environment_id, stack_id, api, current_directory):
        self.rancher = Rancher(rancher_url, environment_id, stack_id, api)
        self.current_directory = current_directory


class DeployScenario(Scenario):

    def get_settings(self):
        return self.rancher.get_configs()

    def deploy(self):
        current_state = self.rancher.check_state()
        print('Current state is: {}'.format(current_state))
        settings = self.get_settings()
        response = self.rancher.upgrade_stack(settings)
        if 'transitioning' in response and response['transitioning'] == 'yes':
            print('Stack is upgrading, please wait...')
        else:
            print('Something went wrong: {}'.format(response['code']))
        current_state = self.rancher.check_state()
        while current_state == 'upgrading':
            print('Stack in upgrading, please wait...')
            time.sleep(10)
            current_state = self.rancher.check_state()
        print('Upgrading is done. Finishing upgrade now.')
        time.sleep(5)
        self.rancher.finish_upgrade()
        current_state = self.rancher.check_state()
        while current_state == 'finishing-upgrade':
            print('Stack in finishing upgrade, please wait...')
            time.sleep(10)
            current_state = self.rancher.check_state()
        print('Done :) (status: {})'.format(current_state))


class CustomDeployScenario(DeployScenario):

    def get_settings(self):
        with open('{}/docker_compose.yaml'.format(self.current_directory), 'r', newline='\r\n') as docker_compose_file:
            docker_compose = docker_compose_file.read().replace('\n', '\r\n')
        with open('{}/rancher_compose.yaml'.format(self.current_directory), 'r', newline='\r\n') \
                as rancher_compose_file:
            rancher_compose = rancher_compose_file.read().replace('\n', '\r\n')
        return dict(dockerCompose=docker_compose, rancherCompose=rancher_compose)


class GetConfigsScenario(Scenario):

    def get_configs(self):
        configs = self.rancher.get_configs()
        docker_compose = configs['dockerCompose'].replace('\r\n', '\n')
        rancher_compose = configs['rancherCompose'].replace('\r\n', '\n')
        with open('{}/docker_compose.yaml'.format(self.current_directory), 'w', newline='\n') as docker_compose_file:
            docker_compose_file.write(docker_compose)
        with open('{}/rancher_compose.yaml'.format(self.current_directory), 'w', newline='\n') as rancher_compose_file:
            rancher_compose_file.write(rancher_compose)
